#include <iostream>
#include <string>
#define MAX 20
using namespace std;

void push(char);
char pop();
string in2postfix(string);
int priority(char);
bool isFull();
bool isEmpty();
int stk[MAX];
int top = -1;
void cetak();

int main(){
    string infix, postfix;
    cin >> infix;
    postfix = in2postfix(infix);
    cetak();

    return 0;
}

bool isFull(){
    if(top == MAX-1){
        return true;
    } else {
        return false;
    }
}

bool isEmpty(){
    if(top == -1){
        return true;
    } else {
        return false;
    }
}

void push(char oper){
    if(isFull()){
        cout << "Stack Penuh" << "\n";
    } else {
        top++;
        stk[top] = oper;
    }
}

char pop(){
    char ch;
    if(isEmpty()){
        cout << "Stack Kosong" << "\n";
    } else {
        ch = stk[top];
        stk[top] = '\0';
        top--;
        return ch;
    }
}

string in2postfix(string infix){
    int i = 0;
    string pst = "";

    //cek dengan looping selama akhir string ada isinya
    while(infix[i] != '\0'){
        //cek jika ketemu alfabet
        if(infix[i] >= 'a' && infix[i] <= 'z'){
            pst.insert(pst.end(), infix[i]);
            i++;

        } //jika ketemu kurung buka simpan ke stack
        else if (infix[i] == '(' || infix[i] == '{' || infix[i] == '['){
            push(infix[i]);
            i++;
        } // jika ketemu kurung tutup pop stack
        else if (infix[i] == ')' || infix[i] == '}' || infix[i] == ']'){
            if(infix[i] == ')'){
                    while(stk[top] != '('){
                        pst.insert(pst.begin(), pop());
                    }
                    pop();
                    i++;
            }
            if(infix[i] == '}'){
                    while(stk[top] != '{'){
                        pst.insert(pst.begin(), pop());
                    }
                    pop();
                    i++;
            }
            if(infix[i] == ']'){
                    while(stk[top] != '['){
                        pst.insert(pst.begin(), pop());
                    }
                    pop();
                    i++;
            }
        }
        else {
            if(top == -1){
                push(infix[i]);
                i++;
            }
            else if (priority(infix[i]) <= priority(stk[top])){
                pst.insert(pst.begin(), pop());

                while(priority(stk[top]) == priority(infix[i])){
                    pst.insert(pst.begin(), pop());
                    if(top < 0){
                        break;
                    }
                }
                push(infix[i]);
                i++;
            }
            else if (priority(infix[i]) > priority(stk[top])){
                push(infix[i]);
                i++;
            }
        }
    }
    while(top != -1){
        pst.insert(pst.begin(), pop());
    }
    cout << "Hasil konversi ke prefix: " << pst;
    return pst;
}

int priority(char alpha){
    if (alpha == '+' || alpha == '-'){
        return 3;
    }
    if (alpha == '*' || alpha == '/'){
        return 2;
    }
    if (alpha == '$'){
        return 1;
    }
    return alpha;
}

void cetak(){
    for(int i = 0; i <= top; i++){
        cout << stk[i] << " ";
    }
}

